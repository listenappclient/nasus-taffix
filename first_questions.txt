Pwd for randomization file (Up to subject 217): NAS242

Taffix clinical study questions:


1. Timing of positive COVID19 diagnosis among the Taffix and Placebo groups
2. For each symptom in the eCRF -  distribution, severity and timing among the Taffix and Placebo groups,
3. For the total reported symptoms in the eCRF- distribution, severity and timing among the Taffix and Placebo groups,
4. Loss of working days reports and timing among the Taffix and Placebo groups
5. Do we identify any subgroups? for instance- woman vs. man? age? baseline medical condition?
6. Subjects who came to perform PCR test- what were their final COVID results? other symptoms?
7. Two subjects who were hospitalized- are they Taffix or Placebo?  